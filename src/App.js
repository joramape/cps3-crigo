import AppNavbar from './components/AppNavbar';
import AllProducts from './pages/AllProducts';
import Myaccount from './pages/Myaccount';
import Shop from './pages/Shop';
import Checkout from './pages/Checkout';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Home from './pages/Home';
import Error from './pages/Error';
import Dashboard from './Admin/Dashboard';
import CreateProduct from './Admin/CreateProduct';
import Product from './Admin/Product';
import ProductDetails from './Admin/ProductDetails';
import ProductUpdate from './Admin/ProductUpdate';
import ProductActive from './Admin/ProductActive';
import ProductArchive from './Admin/ProductArchive';
import Activate from './Admin/Config/Activate';
import Archive from './Admin/Config/Archive';
import Adminuser from './Admin/Adminuser';
import AboutUs from './pages/About';
import CartCheckout from './pages/CartCheckout';

import './App.css';

import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

function App() {
  const [user, setUser] = useState([]);
  const [cart, setCart] = useState([]);

  const addToCart = (product) => {
    const updatedCart = [...cart, product];
    setCart(updatedCart);
  };

  const removeCart = (index) => {
    const updatedCart = [...cart];
    updatedCart.splice(index, 1);
    setCart(updatedCart);
  };

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // User is logged in
        if (typeof data._id !== 'undefined') {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
            firstName: data.firstName,
            lastName: data.lastName,
          });

          // User is logged out
        } else {
          setUser({
            id: null,
            isAdmin: null,
            firstName: null,
            lastName: null,
          });
        }
      });
  }, []);

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar carts={cart} removeCart={removeCart} />
          <Routes>
            <Route path="/" element={<Home addToCart={addToCart} />} />
            <Route path="/shop-products/" element={<Home />} />
            <Route
              path="/allproduct/"
              element={<AllProducts addToCart={addToCart} />}
            />
            <Route path="/about" element={<AboutUs />} />
            <Route path="/my-account/" element={<Myaccount />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/create-product" element={<CreateProduct />} />
            <Route path="/product" element={<Product />} />
            <Route
              path="/product-details/:productId"
              element={<ProductDetails />}
            />
            <Route
              path="/product-update/:productId"
              element={<ProductUpdate />}
            />
            <Route path="/product-active" element={<ProductActive />} />
            <Route path="/product-archive/" element={<ProductArchive />} />
            <Route
              path="/product-reactivate/:productId"
              element={<Activate />}
            />
            <Route
              path="/product-deactivate/:productId"
              element={<Archive />}
            />
            <Route path="/admin-user" element={<Adminuser />} />
            <Route path="/shop/:productId" element={<Shop />} />
            <Route
              path="/checkout/:productId/:quantity"
              element={<Checkout />}
            />
            <Route path="/CartCheckout/:cartId" element={<CartCheckout />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Router>
      </UserProvider>
    </>
  );
}
export default App;
