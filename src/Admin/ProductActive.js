import {
  Button,
  Row,
  Breadcrumb,
  Table,
  Modal,
  Container,
} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function Product() {
  const [products, setProducts] = useState([]);

  const productData = () => {
    return fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then((res) => res.json())
      .then((data) => setProducts(data));
  };

  useEffect(() => {
    productData();
  }, []);

  return (
    <div id="main" className="mt-5">
      <Row>
        <Breadcrumb>
          <h2>Product Active</h2>
        </Breadcrumb>
        <hr />
        <Table bordered hover>
          <thead style={{ backgroundColor: 'black' }} className="text-white">
            <tr>
              <th>Product</th>
              <th>Description</th>
              <th>Size</th>
              <th>Color</th>
              <th>Price</th>
              <th>End User</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {products.map((prod) => (
              <tr>
                <td key={prod.id}>{prod.productName}</td>
                <td key={prod.id}>{prod.description}</td>
                <td key={prod.id}>{prod.size}</td>
                <td key={prod.id}>{prod.color}</td>
                <td key={prod.id}>{prod.price}</td>
                <td key={prod.id}>{prod.endUser}</td>
                <td>
                  <>
                    <Button
                      size="sm"
                      variant="info"
                      type="submit"
                      as={Link}
                      to={`/product-details/${prod._id}`}
                    >
                      Details
                    </Button>
                    <Button
                      size="sm"
                      className="my-1 mx-1"
                      variant="danger"
                      type="submit"
                      as={Link}
                      to={`/product-deactivate/${prod._id}`}
                    >
                      Deactivate
                    </Button>
                  </>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Row>
    </div>
  );
}
