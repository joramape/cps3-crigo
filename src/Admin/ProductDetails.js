import {
  Alert,
  Breadcrumb,
  Row,
  Badge,
  ListGroup,
  Button,
} from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';

export default function ProductDetails() {
  const { productId } = useParams();

  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [size, setSize] = useState('');
  const [color, setColor] = useState('');
  const [price, setPrice] = useState('');
  const [endUser, setendUser] = useState('');
  const [isActive, setIsActive] = useState('');

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProductName(data.productName);
        setDescription(data.description);
        setSize(data.size);
        setColor(data.color);
        setPrice(data.price);
        setendUser(data.endUser);

        console.log(isActive);
        if (data.isActive === true) {
          setIsActive('Available');
        } else {
          setIsActive('Not Available');
        }
      });
  }, [productId]);

  return (
    <div id="main" className="mt-5">
      <Row className="mt-5 mx-auto">
        <Breadcrumb>
          <h2>Product Details</h2>
        </Breadcrumb>
        <hr />
        <Alert variant="success" style={{ width: '60%' }}>
          <Alert.Heading>{productName}</Alert.Heading>
          <p>
            <ListGroup.Item className="">
              <b>Description : </b> {description}
            </ListGroup.Item>
            <ListGroup.Item>
              <b>Size :</b> {size}
            </ListGroup.Item>
            <ListGroup.Item>
              <b>Color :</b> {color}
            </ListGroup.Item>
            <ListGroup.Item>
              <b>Price :</b> {price}
            </ListGroup.Item>
            <ListGroup.Item>
              <b>End User :</b> {endUser}
            </ListGroup.Item>
          </p>
          <hr />
          <p className="mb-0">
            <ListGroup.Item>
              <b>Status :</b>
              {isActive == 'Available' ? (
                <Badge className="mx-3" bg="primary">
                  {isActive}{' '}
                </Badge>
              ) : (
                <>
                  <Badge className="mx-3" bg="danger">
                    {isActive}{' '}
                  </Badge>
                </>
              )}
            </ListGroup.Item>
          </p>
          <Button className="mt-3" variant="primary">
            Activate
          </Button>
          <Button className="mt-3 mx-3" variant="danger">
            Archive
          </Button>
        </Alert>
      </Row>
    </div>
  );
}
