import { Tab, Container, Nav, Col, Row } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { useContext } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faShoppingBasket,
  faArrowDown,
  faMap,
  faUser,
  faLock,
  faDashboard,
} from '@fortawesome/free-solid-svg-icons';

import Orders from './Orders';

export default function Myaccount() {
  const { user } = useContext(UserContext);

  return (
    <Container>
      <Row style={{ marginBottom: 200 }}>
        <Col xs="12" className="mt-5">
          <h1 className="mb-5">My Account</h1>
          <hr />
        </Col>

        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
          <Row>
            <Col sm={3}>
              <Nav variant="pills" className="flex-column">
                <Nav.Link eventKey="dashboard">
                  <FontAwesomeIcon icon={faDashboard} className="mx-3" />
                  Dashboard
                </Nav.Link>
                <hr />
                <Nav.Link eventKey="orders">
                  <FontAwesomeIcon icon={faShoppingBasket} className="mx-3" />
                  Orders
                </Nav.Link>
                <hr />
                <Nav.Link eventKey="downloads">
                  <FontAwesomeIcon icon={faArrowDown} className="mx-3" />{' '}
                  Downloads
                </Nav.Link>
                <hr />
                <Nav.Link eventKey="address">
                  <FontAwesomeIcon icon={faMap} className="mx-3" /> Address
                </Nav.Link>
                <hr />
                <Nav.Link eventKey="account">
                  <FontAwesomeIcon icon={faUser} className="mx-3" /> Accound
                  Details
                </Nav.Link>
                <hr />
                <Nav.Link as={NavLink} to="/logout">
                  <FontAwesomeIcon icon={faLock} className="mx-3" /> Logout
                </Nav.Link>
              </Nav>
            </Col>
            <Col sm={9}>
              <Tab.Content className="mx-5">
                <Tab.Pane eventKey="dashboard">
                  <span>
                    Hello!{' '}
                    <b>
                      {user.firstName} {user.lastName}
                    </b>
                  </span>
                  <p className="mt-5">
                    From your account dashboard you can view your recent orders,
                    manage your shipping and billing addresses, and edit your
                    password and account details.
                  </p>
                </Tab.Pane>
                <Tab.Pane eventKey="orders">
                  <Orders />
                </Tab.Pane>
                <Tab.Pane eventKey="downloads"></Tab.Pane>
                <Tab.Pane eventKey="address"></Tab.Pane>
                <Tab.Pane eventKey="account"></Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </Row>
    </Container>
  );
}
