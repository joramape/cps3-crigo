import { Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Error() {
  return (
    <Container className="mt-5" style={{ marginBottom: '15%' }}>
      <h2 className="text-danger">Error 404 - Page Not Found!</h2>
      <h5>The page you are looking for cannot be found</h5>
      <Button as={Link} to="/" className="mt-5">
        Back
      </Button>
    </Container>
  );
}
