import {
  // ... other imports
  Container,
  Row,
  Col,
  Card,
  Button,
  Form,
  InputGroup,
} from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

export default function Shop() {
  const { user } = useContext(UserContext);
  const { productId } = useParams();
  const Navigate = useNavigate();

  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [size, setSize] = useState('');
  const [color, setColor] = useState('');
  const [price, setPrice] = useState('');
  const [endUser, setendUser] = useState('');
  const [photos, setPhotos] = useState('');

  const [quantity, setQuantity] = useState(1);

  const inc = () => {
    setQuantity(quantity + 1);
  };

  const dec = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProductName(data.productName);
        setDescription(data.description);
        setSize(data.size);
        setColor(data.color);
        setPrice(data.price);
        setendUser(data.endUser);
        setPhotos(data.photos);
      });
  }, [productId]);

  return (
    <Container className="my-5">
      <Row>
        <Col lg={6} md={6} sm={12}>
          <Card>
            <Card.Img className="img-fluid" src={photos} alt={productName} />
          </Card>
        </Col>
        <Col lg={6} md={6} sm={12}>
          <h1>{productName}</h1>
          <h3>₱ {price}</h3>
          <p className="mt-3">
            <b>Description:</b> {description}
          </p>
          <p className="mt-2">
            <b>Size:</b> {size}
          </p>
          <p className="mt-2">
            <b>Color:</b> {color}
          </p>
          <p className="mt-2">
            <b>End User:</b> {endUser}
          </p>

          <hr />

          <div className="d-flex align-items-center">
            <p>
              <b>{productName}</b>
            </p>
            <p className="ml-auto">₱ {price}</p>
          </div>

          <hr />

          <div className="d-flex align-items-center">
            <span>Subtotal:</span>
            <h2 className="ml-auto">₱ {price * quantity}</h2>
          </div>

          <Form.Group controlId="quantity">
            <Form.Label>Quantity:</Form.Label>
            <InputGroup>
              <Button onClick={dec} variant="danger">
                -
              </Button>
              <Form.Control
                type="text"
                value={quantity}
                readOnly
                className="text-center"
              />
              <Button onClick={inc} variant="success">
                +
              </Button>
            </InputGroup>
          </Form.Group>

          <Button
            as={Link}
            to={`/checkout/${productId}/${quantity}`}
            variant="dark"
            className="mt-3"
          >
            PROCEED TO CHECKOUT
          </Button>
        </Col>
      </Row>
    </Container>
  );
}
