import React, { useState, useEffect } from 'react';
import {
  Nav,
  Button,
  Row,
  Breadcrumb,
  Card,
  Col,
  Container,
} from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import '../App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons';

export default function Home({ addToCart }) {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);

  const productData = () => {
    return fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then((res) => res.json())
      .then((data) => setProducts(data));
  };

  useEffect(() => {
    productData();
  }, []);

  return (
    <>
      <div className="banner-container" style={{ backgroundColor: '#3498db' }}>
        <div className="banner-content">
          <h1 className="banner-title">Creative & Inspire... Go!</h1>
          <p className="banner-subtitle">Discover Our Amazing Collection</p>
          <Button
            as={NavLink}
            to="/allproduct"
            size="lg"
            className="banner-button"
            variant="outline-secondary"
          >
            Explore All Products
          </Button>
        </div>
      </div>
      <Container>
        <Breadcrumb className="mt-5">
          <h1 className="mx-auto">FEATURED PRODUCTS</h1>
        </Breadcrumb>
        <hr />
        <Row xs={1} sm={2} md={3} lg={4} xl={4}>
          {products.map((prod) => (
            <Col key={prod._id} className="mt-2 mb-4">
              <Card id="conts" style={{ width: '100%', height: '100%' }}>
                <Card.Img
                  style={{
                    width: '100%',
                    height: '17rem',
                    objectFit: 'cover',
                  }}
                  className="images"
                  variant="top"
                  src={prod.photos}
                />
                <div className="middles">
                  <Button
                    variant="dark"
                    size="sm"
                    as={Link}
                    to={`/shop/${prod._id}`}
                    className="texts"
                  >
                    SELECT OPTION
                  </Button>
                  <Button
                    variant="secondary"
                    size="sm"
                    className="texts mt-2"
                    onClick={() => addToCart(prod)}
                  >
                    ADD TO CART
                  </Button>
                </div>
                <Card.Body>
                  <Card.Title>{prod.name}</Card.Title>
                  <Card.Text>{prod.description}</Card.Text>
                  <Card.Text className="text-muted">
                    Price: ${prod.price}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>

      <footer className="bg-dark text-light py-4">
        <Container>
          <Row>
            <Col md={4}>
              <h4>Contact Us</h4>
              <p>
                <FontAwesomeIcon icon={faEnvelope} />{' '}
                <a href="mailto:contact@example.com" className="text-light">
                  contact@example.com
                </a>
              </p>
              <p>
                <FontAwesomeIcon icon={faPhone} /> +123 456 7890
              </p>
            </Col>
            <Col md={4}>
              <h4>Follow Us</h4>
              <a href="#" className="text-light">
                <FontAwesomeIcon icon={faFacebook} /> Facebook
              </a>
              <br />
              <a href="#" className="text-light">
                <FontAwesomeIcon icon={faTwitter} /> Twitter
              </a>
            </Col>
            <Col md={4}>
              <h4>Quick Links</h4>
              <Nav className="flex-column">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/about">About Us</Nav.Link>
                {/* Add more links as needed */}
              </Nav>
            </Col>
          </Row>
          <hr className="my-3" />
          <p className="text-center">
            &copy; {new Date().getFullYear()} Your Company. All rights reserved.
          </p>
        </Container>
      </footer>
    </>
  );
}
