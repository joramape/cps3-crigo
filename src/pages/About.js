import React from 'react';
import { Container, Row, Col, Nav } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons';

export default function AboutUs() {
  return (
    <div className="mb-4">
      <div style={{ backgroundColor: '#3498db' }} className="text-white py-5">
        <Container>
          <Row>
            <Col>
              <h1 className="display-4 text-center">About Us</h1>
            </Col>
          </Row>
        </Container>
      </div>

      <Container className="my-5">
        <Row>
          <Col>
            <h2 className="mb-4">Our Story</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
              vel turpis vel augue sodales malesuada eu nec elit. Phasellus
              semper nunc id libero blandit, ac vulputate justo lacinia. Duis
              vitae quam eu odio luctus feugiat vel id ex. Suspendisse potenti.
              Integer ac est ut odio hendrerit condimentum in in massa.
            </p>
            <p>
              Sed lacinia purus ac felis dictum, in congue urna ullamcorper.
              Maecenas non ex at purus tincidunt venenatis vitae non odio. Sed
              auctor sapien vel congue blandit.
            </p>
          </Col>
        </Row>
      </Container>

      <footer className="bg-dark text-light py-4">
        <Container>
          <Row>
            <Col md={4}>
              <h4>Contact Us</h4>
              <p>
                <FontAwesomeIcon icon={faEnvelope} />{' '}
                <a href="mailto:contact@example.com" className="text-light">
                  contact@example.com
                </a>
              </p>
              <p>
                <FontAwesomeIcon icon={faPhone} /> +123 456 7890
              </p>
            </Col>
            <Col md={4}>
              <h4>Follow Us</h4>
              <a href="#" className="text-light">
                <FontAwesomeIcon icon={faFacebook} /> Facebook
              </a>
              <br />
              <a href="#" className="text-light">
                <FontAwesomeIcon icon={faTwitter} /> Twitter
              </a>
            </Col>
            <Col md={4}>
              <h4>Quick Links</h4>
              <Nav className="flex-column">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/about">About Us</Nav.Link>
                {/* Add more links as needed */}
              </Nav>
            </Col>
          </Row>
          <hr className="my-3" />
          <p className="text-center">
            &copy; {new Date().getFullYear()} Your Company. All rights reserved.
          </p>
        </Container>
      </footer>
    </div>
  );
}
