import React, { useState, useEffect } from 'react';
import { Nav, Container, Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons';

export default function AllProducts({ addToCart }) {
  const [products, setProducts] = useState([]);

  const productData = (e) => {
    return fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then((res) => res.json())
      .then((data) => setProducts(data));
  };

  useEffect(() => {
    productData();
  }, []);

  return (
    <div>
      <Container>
        <h1 className="mt-5 text-center">All PRODUCTS</h1>
        <hr />

        <Row>
          {products.map((prod) => (
            <Col key={prod._id} className="mt-2 mb-4">
              <Card id="conts" style={{ width: '100%', height: '100%' }}>
                <Card.Img
                  style={{
                    width: '100%',
                    height: '17rem',
                    objectFit: 'cover',
                  }}
                  className="images"
                  variant="top"
                  src={prod.photos}
                />
                <div className="middles">
                  <Button
                    variant="dark"
                    size="sm"
                    as={Link}
                    to={`/shop/${prod._id}`}
                    className="texts"
                  >
                    SELECT OPTION
                  </Button>
                  <Button
                    variant="secondary"
                    size="sm"
                    className="texts mt-2"
                    onClick={() => addToCart(prod)}
                  >
                    ADD TO CART
                  </Button>
                </div>
                <Card.Body>
                  <Card.Title>{prod.name}</Card.Title>
                  <Card.Text>{prod.description}</Card.Text>
                  <Card.Text className="text-muted">
                    Price: ${prod.price}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>

      <footer className="bg-dark text-light py-4">
        <Container>
          <Row>
            <Col md={4}>
              <h4>Contact Us</h4>
              <p>
                <FontAwesomeIcon icon={faEnvelope} />{' '}
                <a href="mailto:contact@example.com" className="text-light">
                  contact@example.com
                </a>
              </p>
              <p>
                <FontAwesomeIcon icon={faPhone} /> +123 456 7890
              </p>
            </Col>
            <Col md={4}>
              <h4>Follow Us</h4>
              <a href="#" className="text-light">
                <FontAwesomeIcon icon={faFacebook} /> Facebook
              </a>
              <br />
              <a href="#" className="text-light">
                <FontAwesomeIcon icon={faTwitter} /> Twitter
              </a>
            </Col>
            <Col md={4}>
              <h4>Quick Links</h4>
              <Nav className="flex-column">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/about">About Us</Nav.Link>
                {/* Add more links as needed */}
              </Nav>
            </Col>
          </Row>
          <hr className="my-3" />
          <p className="text-center">
            &copy; {new Date().getFullYear()} Your Company. All rights reserved.
          </p>
        </Container>
      </footer>
    </div>
  );
}
