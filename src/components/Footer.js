import React from 'react';
import { Col, Container, Nav, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFacebookSquare,
  faInstagramSquare,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';

export default function App() {
  return (
    <div>
      <footer className="bg-dark text-light py-4">
        <Container>
          <Row>
            <Col md={4}>
              <h5>Contact Us</h5>
              <p>Email: contact@example.com</p>
              <p>Phone: +123 456 7890</p>
            </Col>
            <Col md={4}>
              <h5>Follow Us</h5>
              <a href="#" className="text-light">
                Facebook
              </a>
              <br />
              <a href="#" className="text-light">
                Twitter
              </a>
            </Col>
            <Col md={4}>
              <h5>Quick Links</h5>
              <Nav className="flex-column">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/about">About Us</Nav.Link>
                {/* Add more links as needed */}
              </Nav>
            </Col>
          </Row>
          <hr />
          <p className="text-center">
            &copy; {new Date().getFullYear()} Your Company. All rights reserved.
          </p>
        </Container>
      </footer>
    </div>
  );
}
