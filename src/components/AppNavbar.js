import {
  Container,
  Nav,
  Navbar,
  NavDropdown,
  Modal,
  Button,
} from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import '../App.css';
import UserContext from '../UserContext';
import { useContext, React, useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart, faUser } from '@fortawesome/free-solid-svg-icons';

export default function AppNavbar({ carts, removeCart }) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [cartId, setCartId] = useState([]);

  const { user } = useContext(UserContext);
  useEffect(() => {
    document.title = 'Crigo Leather |';
  }, []);
  const navbarStyle = {
    backgroundColor: '#333',
  };
  const brandStyle = {
    fontSize: '36px',
    fontFamily: 'Arial, sans-serif',
    color: '#ffc107',
    fontWeight: 'bold',
    textShadow: '2px 2px 4px rgba(0, 0, 0, 0.3)',
  };

  if (user.id === null) {
    return (
      <Navbar sticky="top" expand="lg" style={navbarStyle} variant="dark">
        <Container>
          <Navbar.Brand as={NavLink} to="/" style={brandStyle}>
            Crigo
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mx-auto">
              <Nav.Link as={NavLink} to="/" className="#action/3.1">
                Home
              </Nav.Link>
              <Nav.Link as={NavLink} to="/allproduct" className="#action/3.1">
                All Product
              </Nav.Link>
              <Nav.Link as={NavLink} to="/about" className="#action/3.1">
                About Us
              </Nav.Link>
              <NavDropdown title="FAQS" id="collasible-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Another action
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">
                  Something
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">
                  Separated link
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <Nav>
              <Nav.Link as={NavLink} to="/login" className="#action/3.1">
                Login
              </Nav.Link>
              <Nav.Link>
                <span className="p-2 position-relative" onClick={handleShow}>
                  <span className="position-absolute start-100 translate-middle badge rounded-pill bg-secondary">
                    {carts ? carts.length : 0}
                  </span>
                  <FontAwesomeIcon
                    icon={faShoppingCart}
                    style={{ color: 'white' }}
                  />
                </span>
              </Nav.Link>
            </Nav>
            <Modal show={show} onHide={handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Cart</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <ol className="p-2">
                  {carts.map((item, index) => (
                    <div
                      className="p-1 alert alert-primary alert-dismissible fade show"
                      role="alert"
                      key={index}
                    >
                      <li>
                        {item.productName} - {item.price}
                      </li>
                      <button
                        type="button"
                        className="p-1 btn-close btn-sm"
                        onClick={() => removeCart(index)}
                      />
                    </div>
                  ))}
                </ol>
              </Modal.Body>
              <Modal.Footer>
                <Button
                  as={Link}
                  to={`/CartCheckout/${carts.map((items) => [items._id])}`}
                  onClick={handleClose}
                >
                  Proceed to Checkout
                </Button>
              </Modal.Footer>
            </Modal>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }

  if (user.isAdmin === true) {
    return (
      <>
        <Navbar sticky="top" bg="dark" variant="dark">
          <Navbar.Brand className="mx-5">Admin</Navbar.Brand>
          <Container>
            <Navbar.Toggle />
            <Navbar.Collapse className="justify-content-end">
              <Navbar.Text>
                <Nav.Link as={NavLink} to="/logout">
                  Logout
                </Nav.Link>
              </Navbar.Text>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        <div className="sidebar">
          <Nav.Link className="active">
            <h5>
              {' '}
              <FontAwesomeIcon icon={faUser} className="mx-1" />{' '}
              <b>
                {user.firstName} {user.lastName}
              </b>
            </h5>
          </Nav.Link>
          <Nav.Link as={NavLink} to="/dashboard">
            Dashboard
          </Nav.Link>
          <Nav.Link as={NavLink} to="/create-product">
            Create Product
          </Nav.Link>
          <Nav.Link as={NavLink} to="/product">
            All Product
          </Nav.Link>
          <Nav.Link as={NavLink} to="/product-active">
            Active Product
          </Nav.Link>
          <Nav.Link as={NavLink} to="/product-archive">
            Archive Product
          </Nav.Link>
          <Nav.Link as={NavLink} to="/admin-user">
            Admin - User
          </Nav.Link>
        </div>
      </>
    );
  }

  if (user.isAdmin === false) {
    return (
      <Navbar
        sticky="top"
        collapseOnSelect
        expand="xl"
        bg="dark"
        variant="dark"
      >
        <Container>
          <Navbar.Brand as={NavLink} to="/shop-products/">
            Crigo
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mx-auto ">
              <Nav.Link as={NavLink} to="/shop-products/">
                Shop
              </Nav.Link>
              <Nav.Link href="#pricing">About Us</Nav.Link>
            </Nav>
            <Nav>
              <Nav.Link as={NavLink} to="/my-account/">
                My Account
              </Nav.Link>
              <Nav.Link eventKey={2} href="#cart"></Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }
}
